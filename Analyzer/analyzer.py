import re
import string
import json
from collections import Counter
import operator

to_json = lambda x: json.dumps(x, indent = 4)
def get_regex_apperances(s,regs,nm):
    res = set()
    for r in regs:
        p = re.compile(r)
        for m in p.finditer(s):
            res.add((m.start(),m.end()))

    res = [s[i:j] for (i,j) in res]
    d_temp = dict(Counter(res))

    if nm == "Attack Groups":
        ag = max(d_temp.items(), key=operator.itemgetter(1))[0]
        d = ag
    else:
        d = list(d_temp.keys())
    

    


    return d

def extract_doain_name(url):
    url = url.split("/")[2]
    url = url.split(".")
    return url[1] if "www" in url[0] else url[0]

def extract_mails(s):
    pass

def get_keywords(s,kws):
    res = dict()
    for w in kws:
        count = s.count(w)
        if count > 0: 
            res[w] = count
    return res


with open("regexes.json","r") as f:
    REGS_DICT = json.load(f)
with open("keywords.txt","r") as f:
    KEYWORDS = f.readlines()
    KEYWORDS = list(map(lambda s: s.lower().replace("\n",""), KEYWORDS))

def summerize(d):
    global REGS_DICT, KEYWORDS
    domain_name = extract_doain_name(d["url"])
    content = d["raw_content"].lower()
    detection = dict()
    detection["Keywords"] = get_keywords(content,KEYWORDS)

    for nm, regs in REGS_DICT.items():
        detection[nm] = get_regex_apperances(content,regs, nm)

    #detection['Emails'] = extract_mails(content)

    #delete urls that are in domain_name
    to_del = set(filter(lambda url: extract_doain_name(url) == domain_name, detection["URLs"]))
    for url in to_del:
        del detection["URLs"][url]

    return detection

with open("data.json","r") as f:
    data = json.load(f)

ii = 5
print(data[ii]["url"])
res = summerize(data[ii])
print(to_json(res))