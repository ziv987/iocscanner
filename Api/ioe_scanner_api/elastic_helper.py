from elasticsearch import Elasticsearch


class ElasticClient:
    def __init__(self, host, port=9200):
        self.host = host
        self.port = port

    def __enter__(self):
        self.client = Elasticsearch(host=self.host, port=self.port)
        return self

    def insert_article(self, index, data, id=None):
        if id:
            self.client.index(index=index, body=data, id=id)
        else:
            self.client.index(index=index, body=data)

    def get_article(self, index, article_id):
        return self.client.get(index=index, id=article_id)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.client = None
