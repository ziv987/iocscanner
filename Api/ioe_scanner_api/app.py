from flask import Flask, request
from flask_restplus import Api, Resource, fields
from elastic_helper import ElasticClient
from typing import Dict
import json

flask_app = Flask(__name__)
app = Api(app=flask_app,
          version="1.0",
          title="IOEScanner API",
          description="BAM",
          doc='/api/documentation',
          prefix='/api')

ELASTIC_HOST = '35.246.231.145'
ELASTIC_PORT = 9200
ELASTIC_ARTICLES_INDEX = 'articles'

name_space = app.namespace('article', description='Process files - articles')



article_schema = app.model('Article',
                           {'article_name': fields.String(required=True,
                                                          description="Article id",
                                                          help="Cannot be blank."),

                            'iocs': fields.Raw(required=True,
                                               description="IOCs",
                                               help="Cannot be blank."),
                            'article_data': fields.String(required=True,
                                                          description="Article's data",
                                                          help="Data should be encoded with base64.")
                            })


@name_space.route("/insert_article")
class MainClass(Resource):

    # @app.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
    #          params={'article_id': 'Specify the Id associated with the article'})
    # def get(self, article_id):
    #     try:
    #         with ElasticClient(host=ELASTIC_HOST, port=ELASTIC_HOST) as es:
    #             return es.get_article(index=ELASTIC_ARTICLES_INDEX, article_id=article_id)
    #
    #
    #     except Exception as e:
    #         name_space.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")
    #     except Exception as e:
    #         name_space.abort(400, e.__doc__, status="Could not retrieve information", statusCode="400")

    @app.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'})
    @app.expect(article_schema)
    def post(self):
        # -- TODO: PARSE ARTICLE ---

        try:
            request_data = request.json
            iocs = request_data.get('iocs')
            iocs['article_name'] = request_data.get('article_name')
            if iocs:
                with ElasticClient(host=ELASTIC_HOST, port=ELASTIC_PORT) as es:
                    return es.insert_article(index=ELASTIC_ARTICLES_INDEX, data=iocs)


        except KeyError as e:
            name_space.abort(500, e.__doc__, status=e, statusCode="500")
        except Exception as e:
            name_space.abort(400, e.__doc__, status=e, statusCode="400")


if __name__ == '__main__':
    flask_app.run(debug=True)
