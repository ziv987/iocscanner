import urllib
import urllib2
import json
from HTMLParser import HTMLParser
from htmlentitydefs import name2codepoint
import re

class CrawlerHTMLParser(HTMLParser):
    
    isBlogTitles = False
    hrefs = []
    
    def handle_starttag(self, tag, attrs):
        if tag == 'div':
            attrs_dict = dict(attrs)
            if attrs_dict.get('class') == 'c11v9':
                CrawlerHTMLParser.isBlogTitles=True
        
        if tag == 'a' and CrawlerHTMLParser.isBlogTitles:
            attrs_dict = dict(attrs)
            CrawlerHTMLParser.hrefs.append(attrs_dict['href'])

    def handle_endtag(self, tag):
        if tag == 'div':
            CrawlerHTMLParser.isBlogTitles=False
            
    def handle_data(self, data):
        pass




class MyHTMLParser(HTMLParser):
    
    def __init__(self):
        HTMLParser.__init__(self) 
        self.isH1 = False
        self.isH2 = False
        self.isP = False
        self.h1 = ''
        self.h2 = ''
        self.p = ''
        
    def handle_starttag(self, tag, attrs):
        if tag == 'h1':
            self.isH1 = True
        elif tag == 'h2':
            self.isH2 = True
        elif tag == 'p':
            self.isP = True

    def handle_endtag(self, tag):
        if tag == 'h1':
            self.isH1 = False
        elif tag == 'h2':
            self.isH2 = False
        elif tag == 'p':
            self.isP = False
            
            
    def handle_data(self, data):
        if self.isH1:
            self.h1 = self.h1 + data
        elif self.isH2:
            self.h2 = self.h2 + data
        elif self.isP:
            self.p = self.p + data


parser = CrawlerHTMLParser()

for i in range(2):
    https_str = 'https://www.fireeye.com'
    url = 'https://www.fireeye.com/blog/threat-research/jcr:content/blog/entries.html?initReq=no&startIndex=' + str(9*i) + '&productsCount=0&threatCount=1&execCount=0'
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}

    req = urllib2.Request(url, headers=headers)
    response = urllib2.urlopen(req)
    the_page = response.read()

    parser.feed(the_page)


parsed_htmls = []

for url  in CrawlerHTMLParser.hrefs:
    req = urllib2.Request(https_str+url, headers=headers)
    response = urllib2.urlopen(req)
    the_page_old = response.read()        
    the_page = the_page_old.decode('utf-8')
    parser = MyHTMLParser()
    parser.feed(the_page)
    parsed_htmls.append({'url': https_str+url,
                         'main_title': parser.h1,
                         'sub_title': parser.h2,
                         'raw_content': parser.p,})
    name_of_file = parser.h1.replace('\n', ' ').replace(':', ' ').replace('?', ' ').encode('ascii','ignore') + '.html'
    with open(name_of_file, 'wb') as f:
        f.write(the_page_old)
    
jsn = json.dumps(parsed_htmls)

with open(r'.\data.json', 'w') as outfile:
    json.dump(parsed_htmls, outfile, indent=4)

    


